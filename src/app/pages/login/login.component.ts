import { Component, OnInit } from '@angular/core';
import { FormArray, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { GlobalService } from 'src/app/shared/services/common/global.service';
import { AwsamplifyService } from 'src/app/shared/services/awsamplify/awsamplify.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from '@angular/router';
import { language } from '../../../assets/i18n/en';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [GlobalService, AwsamplifyService]
})
export class LoginComponent implements OnInit {

  //------------------------------------------------------------
  // PUBLIC VARIABLES AND DECLARATIONS
  //------------------------------------------------------------
  public screenLangData;
  public globalLangData;

  // SET MODULE
  public module = 'PAGES';
  public component = "LoginComponent";
  error = "";

  constructor(public _formBuilder: FormBuilder,
    public globalService: GlobalService,
    public amplifyService: AwsamplifyService,
    private toastrService: ToastrService,
    private spinner: NgxSpinnerService,
    public _router: Router
  ) { }

  ngOnInit() {
    this.initializeForm1();
    this.screenLangData = language[this.module][this.component];
  }

  //------------------------------------------------------------
  // FORM & VALIDATIONS
  //------------------------------------------------------------

  // ACCESSABLE VARIABLES
  public loginForm: FormGroup;
  public loginFormErrors = {
    'emailId': '',
    'password': '',
  };

  initializeForm1() {
    // INITIALIZE FORM 1
    this.loginForm = this._formBuilder.group({
      'emailId': ['', [Validators.required, Validators.pattern(this.globalService.vEmailPattern)]],
      'password': ['', [Validators.required]]
    });

    this.loginForm.valueChanges
      .subscribe(data => {
        const form = this.loginForm;
        for (const field in this.loginFormErrors) {
          this.loginFormErrors[field] = '';
          const control = form.get(field);
          if (control && control.dirty && !control.valid) {
            console.log(this.screenLangData);
            const messages = this.screenLangData.loginForm.validationMessages[field];
            console.log(messages);
            for (const key in control.errors) {
              this.loginFormErrors[field] += messages[key] + ' ';
            }
          }
        }
      });

  }

  checkLogin() {
    if (!this.loginForm.valid) {
      this.globalService.markAllDirty(this.loginForm);
      return false;
    }

    this.spinner.show();

    this.amplifyService.signIn(this.loginForm.value.emailId, this.loginForm.value.password).subscribe(user => {
      this.toastrService.success('Login Successfully');
      this._router.navigate(['home/category/1']);
      this.spinner.hide();
    },
      (error) => {
        this.error = error;
        this.toastrService.error('Login Failed');
        this.spinner.hide();
      });

  }
}
