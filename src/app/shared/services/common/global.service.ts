import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { FormArray, FormGroup, Validators, FormBuilder, FormControl, AbstractControl } from '@angular/forms';
import { Http } from '@angular/http';
import * as _ from "lodash";
declare let $: any;

@Injectable()
export class GlobalService {

    // ENABLE/ DISABLE REQUEST RESPONSE LOG
    public enableLog = true;

    // VALIDATION PATTERN   
    public vAlphabetPattern = '^[a-zA-Z]+$'; // Applicable for alphapet field
    public vAlphaNumeric = '[أ-يa-zA-Z-_()0-9]+'; // Applicable for alpha Numeric field    
    public vAlphaNumericWithDot = '[أ-يa-zA-Z-_()0-9.]+'; // Applicable for alpha Numeric with dot  
    public vAlphaNumericWithSpace = '[أ-يa-zA-Z-_()0-9 ]+'; // Applicable for alpha Numeric with space        
    public vNumberPattern = '^[0-9]*$'; // Applicable for Number field
    public vAlphabetWithSpacePattern = '^[a-zA-Z ]+$'; // Applicable for alphapet with space
    public vPhonenumberPattern = '[0-9]+'; // Applicable for phone Number field
    public vEmailPattern = '([a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]{1}[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]*)((@[a-zA-Z\-]{2}[a-zA-Z\-]*)[\\\.](([a-zA-Z]{3}|[a-zA-Z]{2})|([a-zA-Z]{3}|[a-zA-Z]{2}).[a-zA-Z]{2}))'; // Applicable for email field
    public vPasswordPattern = '^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$'; // Password pattern 
    public vUrlPattern = "^(?:https?:\/\/)?(?:[-a-z0-9]+\\.)+[-a-z0-9]+(?:(?:(?:\/[-=&?.#a-z0-9]+)+)\/?)?$";  // Applicable for URL field
    public vVersionPattern = '^(?!.*?[._]{2})[a-zA-Z0-9_.]+$';

    // MODULES
    public modules = {
        "super-admin": "SUPER-ADMIN",
        "admin": "ADMIN",
        "user": "USER"
    }

    //------------------------------------------------------------
    // CONSTRUCTOR
    //------------------------------------------------------------
    constructor(public _router: Router) {
    }


    //------------------------------------------------------------
    // COMMON VALIDATION
    //------------------------------------------------------------
    validation(formControl, fromError, formMsgs) {
        const form = formControl;

        for (const field in fromError) {

            if (_.isArray(fromError[field])) {
                //VALIDATION FOR DYNAMIC FIELDS
                const tempForm = <FormArray>form.controls[field];;
                let tempLength = tempForm.length;

                //CHECK ALL ROWS
                for (let i = 0; i <= tempLength - 1; i++) {
                    const errorMsg = fromError[field][i];
                    for (const dynamicField in errorMsg) {
                        errorMsg[dynamicField] = '';
                        const tmpControl = form.controls[field]['controls'];
                        const control = tmpControl[i].controls[dynamicField];
                        if (control && control.dirty && !control.valid) {
                            const messages = formMsgs.validationMessages[field][dynamicField];
                            for (const key in control.errors) {
                                errorMsg[dynamicField] = messages[key];
                            }
                        }
                    }
                }

            } else {

                //VALIDATION FOR STATIC FIELDS
                fromError[field] = '';
                const control1 = form.get(field);

                if (control1 && control1.dirty && !control1.valid) {
                    const messages1 = formMsgs.validationMessages[field];
                    for (const key in control1.errors) {
                        fromError[field] = messages1[key] + ' ';
                    }
                }
            }
        }
    }
    //------------------------------------------------------------
    // CHECK CONTROLS
    //------------------------------------------------------------
    markAllDirty(control: AbstractControl) {
        if (control.hasOwnProperty('controls')) {
            control.markAsDirty({ onlySelf: true }); // mark group
            let ctrl = <any>control;
            for (let inner in ctrl.controls) {
                this.markAllDirty(ctrl.controls[inner] as AbstractControl);
            }
        }
        else {
            (<FormControl>(control)).updateValueAndValidity();
            (<FormControl>(control)).markAsDirty({ onlySelf: true });
        }
    }

}