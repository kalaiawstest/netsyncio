import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Auth } from 'aws-amplify';
import { BehaviorSubject, Observable, throwError } from "rxjs";
import { fromPromise } from 'rxjs/observable/fromPromise';
import { catchError, map } from 'rxjs/operators';

@Injectable()
export class AwsamplifyService {

    public loggedIn: BehaviorSubject<any> = new BehaviorSubject<any>(null);

    constructor(private route: Router) {
    }

    signIn(email: string, password: string): Observable<any> {
        return fromPromise(Auth.signIn(email, password))
            .pipe(
                map(result => {
                    this.loggedIn.next(true);
                    return result;
                }),
                catchError(error => {
                    this.loggedIn.next(false);
                    return throwError(error);
                })
            );
    }
}