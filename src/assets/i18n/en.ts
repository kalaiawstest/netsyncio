export const language = {
    "PAGES": {
        "LoginComponent": {
            "loginForm": {
                "validationMessages": {
                    "emailId": {
                        "required": "Email is required",
                        "pattern": "Please enter valid email"
                    },
                    "password": {
                        "required": "Password is required"
                    }
                },
                "placeHolders": {
                    "emailId": "Email",
                    "password": "Password"
                },
                "labels": {
                    "rememberMe": "Remember me"
                }
            },
            "labels": {
                "title": "Sign In",
                "forgotPassword": "Forgot Password ?",
                "submit": "Submit"
            }
        }
    }
}